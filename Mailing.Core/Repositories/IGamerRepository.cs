using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mailing.Core.Models;
using RestEase;

namespace Mailing.Core.Repositories
{
    public interface IGamerRepository
    {
         Gamer Get(Guid id);
         IEnumerable<Gamer> GetByGroupId(int groupId);
         Task AddAsync(Gamer gamer);
         Task DeleteAsync(string email);
    }
}