namespace Mailing.Core.Models
{
    public class Message
    {
        public string Subject { get; set; }

        public string Content { get; set; }
    }
}