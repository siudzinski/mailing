﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mailing.Core.Models
{
    /// <summary>
    /// Contains all information required to send an email
    /// </summary>
    public class EmailMessage
    {
        public string EmailSubject { get; set; }

        public string EmailContent { get; set; }

        public string RecipientName { get; set; }

        public string RecipientEmailAddress { get; set; }

        public string SenderName { get; set; }

        public string SenderEmailAddress { get; set; }
    }
}
