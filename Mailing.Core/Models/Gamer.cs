using System;

namespace Mailing.Core.Models
{
    public class Gamer
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }
    }
}