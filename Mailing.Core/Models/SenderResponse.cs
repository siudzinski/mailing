﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mailing.Core.Models
{
    /// <summary>
    /// Response received from sender
    /// </summary>
    public class SenderResponse
    {
        public bool IsSuccessfullyDelivered { get; set; }
    }
}
