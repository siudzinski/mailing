namespace Mailing.Infrastructure.Logs
{
    public interface ILogger
    {
         void Log(string message);
    }
}