using System;

namespace Mailing.Infrastructure.Logs
{
    public class SimplyLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}