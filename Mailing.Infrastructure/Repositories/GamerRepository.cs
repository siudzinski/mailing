using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mailing.Core.Models;
using Mailing.Core.Repositories;
using RestEase;

namespace Mailing.Infrastructure.Repositories
{
    public class GamerRepository : IGamerRepository
    {
        private readonly string ApiKey = "bf2d9f5bcf8565bf544b2c4cc43f65d0ea3fd"; //it's here only for simplicity, it should be store in UserSecrets
        private readonly string RestDbUrl = "https://mailing-107c.restdb.io/rest";

        public Gamer Get(Guid id)
        {
            var api = RestClient.For<IRestdbApi>(RestDbUrl);
            return api.GetAll(new { Id = id }, ApiKey).Result.FirstOrDefault();
        }

        public IEnumerable<Gamer> GetByGroupId(int groupId)
        {
            var api = RestClient.For<IRestdbApi>(RestDbUrl);
            return api.GetAll(new { GroupId = groupId }, ApiKey).Result;
        }

        public Task AddAsync(Gamer gamer)
        {
            var api = RestClient.For<IRestdbApi>(RestDbUrl);
            return api.AddGamerAsync(gamer, ApiKey);
        }

        public Task DeleteAsync(string email)
        {
            var api = RestClient.For<IRestdbApi>(RestDbUrl);
            return api.DeleteGamerAsync(new { Email = email }, ApiKey);
        }
    }
}