using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mailing.Core.Models;
using RestEase;

namespace Mailing.Infrastructure.Repositories
{
    public interface IRestdbApi
    {
        [Get("gamers")]
        Task<IEnumerable<Gamer>> GetAll([Query(QuerySerializationMethod.Serialized)] dynamic q, [Query] string apikey);

        [Post("gamers")]
        Task AddGamerAsync([Body] Gamer gamer, [Query] string apikey);

        [Delete("gamers/*")]
        Task DeleteGamerAsync([Query(QuerySerializationMethod.Serialized)] dynamic q, [Query] string apikey);
    }
}