using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mailing.Core.Models;

namespace Mailing.Infrastructure.Services
{
    public interface IGamerService
    {
        Gamer Get(Guid id);
        IEnumerable<Gamer> GetByGroupId(int groupId);
        Task AddAsync(Gamer gamer);
        Task DeleteAsync(string email);
    }
}