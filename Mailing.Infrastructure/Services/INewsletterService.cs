using System;
using System.Threading.Tasks;
using Mailing.Core.Models;

namespace Mailing.Infrastructure.Services
{
    public interface INewsletterService
    {
        void Send(Guid gamerId, Message message);

        void SendMany(int gamerGroupId, Message message);
    }
}