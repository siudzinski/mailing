using System;
using System.Threading.Tasks;
using Mailing.Core.Models;
using Mailing.Infrastructure.Logs;

namespace Mailing.Infrastructure.Services
{
    public class NewsletterService : INewsletterService
    {
        private readonly string SenderEmailAddress = "newsletter@my-example-company-123.io";
        private readonly string SenderName = "MyExampleCompany123";

        private IMailingService _mailingService;
        private IGamerService _gamerService;
        private ILogger _logger;

        public NewsletterService(IMailingService mailingService, IGamerService gamerService, ILogger logger)
        {
            _mailingService = mailingService;
            _gamerService = gamerService;
            _logger = logger;
        }

        public void Send(Guid gamerId, Message message)
        {
            var gamer = _gamerService.Get(gamerId);
            Send(gamer, message);
        }

        public void SendMany(int gamerGroupId, Message message)
        {
            var gamers = _gamerService.GetByGroupId(gamerGroupId);
            foreach(var gamer in gamers)
            {
                Send(gamer, message);
            }
        }

        private void Send(Gamer gamer, Message message)
        {
            var emailMessage = CreateEmailMessage(gamer, message);

            var result = _mailingService.Send(emailMessage);

            if(!result.IsSuccessfullyDelivered)
            {
                _logger.Log($"Delivering message to {gamer.Email} ended with failure.");
            }
        }

        private EmailMessage CreateEmailMessage(Gamer gamer, Message message)
        {
            return new EmailMessage
            {
                EmailSubject = message?.Subject,
                EmailContent = message?.Content,
                RecipientEmailAddress = gamer?.Email,
                RecipientName = gamer?.Name,
                SenderEmailAddress = SenderEmailAddress,
                SenderName = SenderName
            };
        }
    }
}