using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Mailing.Core.Models;
using Mailing.Core.Repositories;

namespace Mailing.Infrastructure.Services
{
    public class GamerService : IGamerService
    {
        private IGamerRepository _gamerRepository;

        public GamerService(IGamerRepository gamerRepository)
        {
            _gamerRepository = gamerRepository;
        }

        public Gamer Get(Guid id)
        {
            return _gamerRepository.Get(id);
        }

        public IEnumerable<Gamer> GetByGroupId(int groupId)
        {
            return _gamerRepository.GetByGroupId(groupId);
        }

        public Task AddAsync(Gamer gamer)
        {
            gamer.Id = Guid.NewGuid();
            return _gamerRepository.AddAsync(gamer);
        }

        public Task DeleteAsync(string email)
        {
            return _gamerRepository.DeleteAsync(email);
        }
    }
}