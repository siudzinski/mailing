﻿using Mailing.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mailing.Infrastructure.Services
{
    /// <summary>
    /// Interface used to send emails
    /// </summary>
    public interface IMailingService
    {
        SenderResponse Send(EmailMessage message);

        IEnumerable<SenderResponse> SendMany(IEnumerable<EmailMessage> messageList);
    }
}
