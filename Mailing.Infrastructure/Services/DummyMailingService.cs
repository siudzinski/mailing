﻿using Mailing.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mailing.Infrastructure.Services
{
    /// <summary>
    /// Mock implementation of IMailingService (@todo Remove when real service is ready)
    /// </summary>
    public class DummyMailingService : IMailingService
    {
        private readonly Random _random;

        public DummyMailingService()
        {
            _random = new Random();
        }

        public SenderResponse Send(EmailMessage message)
        {
            return new SenderResponse
            {
                IsSuccessfullyDelivered = GetRandomBool()
            };
        }

        public IEnumerable<SenderResponse> SendMany(IEnumerable<EmailMessage> messageList)
        {
            return messageList.Select(
                message => new SenderResponse
                {
                    IsSuccessfullyDelivered = GetRandomBool()
                }
            );
        }

        private bool GetRandomBool()
        {
            return _random.Next(0, 10) != 0;
        }
    }
}
