﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mailing.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Mailing.Infrastructure.Repositories;
using Mailing.Core.Repositories;
using Mailing.Infrastructure.Services;

namespace Mailing.Api.Controllers
{
    [Route("api/newsletter/[controller]")]
    public class GamersController : Controller
    {
        private IGamerService _gamerService;

        public GamersController(IGamerService gamerService)
        {
            _gamerService = gamerService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Gamer gamer)
        {
            await _gamerService.AddAsync(gamer);
            return Ok(gamer);
        }

        [HttpDelete("{email}")]
        public async Task<IActionResult> Delete(string email)
        {
            await _gamerService.DeleteAsync(email);
            return NoContent();
        }
    }
}
