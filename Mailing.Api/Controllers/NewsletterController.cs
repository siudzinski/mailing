﻿using System;
using System.Threading.Tasks;
using Mailing.Core.Models;
using Mailing.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace Mailing.Api.Controllers
{
    [Route("api/[controller]")]
    public class NewsletterController : Controller
    {
        private INewsletterService _newsletterService;

        public NewsletterController(INewsletterService newsletterService)
        {
            _newsletterService = newsletterService;
        }

        [HttpPost("send/{gamerId}")]
        public void Post(Guid gamerId, [FromBody]Message message)
        {
            _newsletterService.Send(gamerId, message);
        }

        [HttpPost("sendmany/{gamerGroupId}")]
        public void Post(int gamerGroupId, [FromBody]Message message)
        {
            _newsletterService.SendMany(gamerGroupId, message);
        }
    }
}